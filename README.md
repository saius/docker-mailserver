Mail server local en Docker. Hostea en localhost en varios puertos.

Sirve tanto para __recibir__ como para __enviar__ mails de testeo.

La casilla creada se puede abrir desde cualquier cliente de mails (tipo Thunderbird).

Esto es básicamente una copia personal y documentada de 
[docker-mailserver](https://github.com/docker-mailserver/docker-mailserver).

## Levantar el server

```
cp .env.example .env
# editar .env a gusto

docker-compose up

# en otra consola
./setup.sh email add user@hostname.local
```

Tener en cuenta que usa certificados TLS autofirmados (self signed cert) por defecto.

Aplicaciones de Node que molesten por esto se puede bypassear (muy inseguramente!)
configurando la variable de entorno `NODE_TLS_REJECT_UNAUTHORIZED` a 0.
